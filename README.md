Dataset : NYPD Arrest Data (Year to Date)
NYC_Arrests.txt = data stored in json dictionary

get_arrest_data.py = script to access several post routes including ...
	
	plot_data : creat a plot of some type of arrest over time
	New_Arrest : post a new arrest to the database
	Arrests_by_race : returns all the arrests of a given race and the pct of total arrest the makeup
	Arrests_by_date : returns all arrests within a date range specified by the user (2019)
	Arrest_statistics : returns statistics about a certain crime as chosen by the user
	
app.py = flask app that handles requests and creates job tickets for worker.py to update

worker.py = scrubs the queue for any new jobs, once one is found worker.py updates the ticket while alsomaking post and get requests with jobs.py to recieve the user specific output as specified by the ticket

jobs.py = does the app's heavy lifting by computing and compiling the output that is requested from the user. This ouput is then sent back to worker.py to store in the job tickets output section

run app.py with command $ flask run -h localhost -p 5000

run jobs.py with command $ flask run -h localhost -p 5002

run worker.py in the background with command $ python worker.py &

run get_arrest_data.py with $ python get_arrest_data.py to get access to most of the routes available

curl localhost:5000/casecount/<crime_index> : returns amount of arrests made for a user specified crime in 2019

curl localhost:5000/get_arrest/<Arrest_key> : returns details of arrest matching the user inputted Arrest_key

quirks of the dataset:
	
	all races are written in all caps and consist of the following 6 categories
		1. WHITE
		2. BLACK
		3. WHITE HISPANIC
		4. BLACK HISPANIC
		5. ASIAN / PACIFIC ISLANDER
		6. AMERICAN INDIAN/ALASKAN NATIVE

	Boroughs are dentoted with a single capital letter
		B = The Bronx
		K = Brooklyn
		M = Manhattan
		S = Staten Island
		Q = Queens

	Crimes are plentiful and are denoted with odd abrevieated names so for the sake of this app, crimes will be referenced with an index number that corresponds to a given crime committed. When asked to enter a crime, a list will be printed out with all the crimes and their respective index. in the case of the route /casecount which is accessed with a curl request, the list of crimes and their indeces are as follows.

		1. "INTOXICATED & IMPAIRED DRIVING"
		2. "THEFT-FRAUD"
		3. "HOMICIDE-NEGLIGENT-VEHICLE"
		4. "CRIMINAL MISCHIEF & RELATED OF"
		5. "ASSAULT 3 & RELATED OFFENSES"
		6. "SEX CRIMES"
		7. "DANGEROUS DRUGS"
		8. "OFFENSES AGAINST PUBLIC ADMINI"
		9. "MISCELLANEOUS PENAL LAW"
		10. "BURGLARY"
		11. "PETIT LARCENY"
		12. "VEHICLE AND TRAFFIC LAWS"
		13. "ROBBERY"
		14. "FELONY ASSAULT"
		15. "DANGEROUS WEAPONS"
		16. "CRIMINAL TRESPASS"
		17. "NYS LAWS-UNCLASSIFIED FELONY"
		18. "OTHER TRAFFIC INFRACTION"
		19. "GRAND LARCENY"
		20. "FRAUDS"
		21. "MURDER & NON-NEGL. MANSLAUGHTE"
		22. "OFFENSES INVOLVING FRAUD"
		23. "ADMINISTRATIVE CODE"
		24. "OFF. AGNST PUB ORD SENSBLTY &"
		25. "FORGERY"
		26. "OFFENSES AGAINST PUBLIC SAFETY"
		27. "POSSESSION OF STOLEN PROPERTY"
		28. "GRAND LARCENY OF MOTOR VEHICLE"
		29. "UNAUTHORIZED USE OF A VEHICLE"
		30. "OTHER OFFENSES RELATED TO THEF"
		31. "ENDAN WELFARE INCOMP"
		32. "RAPE"
		33. "OTHER STATE LAWS"
		34. "OTHER STATE LAWS (NON PENAL LA"
		35. "INTOXICATED/IMPAIRED DRIVING"
		36. "HOMICIDE-NEGLIGENT,UNCLASSIFIE"
		37. "FRAUDULENT ACCOSTING"
		38. "BURGLAR'S TOOLS"
		39. "ARSON"
		40. "OFFENSES AGAINST THE PERSON"
		41. "ALCOHOLIC BEVERAGE CONTROL LAW"
		42. "THEFT OF SERVICES"
		43. "GAMBLING"
		44. "PROSTITUTION & RELATED OFFENSES"
		45. "FOR OTHER AUTHORITIES"
		46. "MOVING INFRACTIONS"
		47. "DISORDERLY CONDUCT"
		48. "AGRICULTURE & MRKTS LAW-UNCLASSIFIED"
		49. ""
		50. "KIDNAPPING & RELATED OFFENSES"
		51. "ANTICIPATORY OFFENSES"
		52. "HARRASSMENT 2"
		53. "OFFENSES RELATED TO CHILDREN"
		54. "JOSTLING"
		55. "OTHER STATE LAWS (NON PENAL LAW)"
		56. "ADMINISTRATIVE CODES"
		57. "CHILD ABANDONMENT/NON SUPPORT"
		58. "DISRUPTION OF A RELIGIOUS SERV"
		59. "LOITERING FOR DRUG PURPOSES"
		60. "NEW YORK CITY HEALTH CODE"
		61. "LOITERING/GAMBLING (CARDS, DIC"
		62. "ESCAPE 3"
		63. "KIDNAPPING"
		64. "NYS LAWS-UNCLASSIFIED VIOLATION"
		65. "UNLAWFUL POSS. WEAP. ON SCHOOL"
		66. "PARKING OFFENSES"

	Sex is denoted by a single capital letter
		M = Male
		F = Female

	Level of Offense is denoted with a single capital letter
		M = Misdemeanor
		F = Felony
		V = Violation

Sample outputs
	
Arrest_statistics

	MURDER & NON-NEGL. MANSLAUGHTE arrests made up 0.47200361574339406% of all NYPD arrests in 2019
	Total arrests: 1013

	Arrests by gender
	Male: 95.55774925962488%
	Female: 4.4422507403751235

	Arrests by borough
	Manhattan: 20.63178677196446%
	The Bronx: 36.92003948667325%
	Brooklyn: 21.816386969397826%
	Queens: 16.38696939782823%
	Staten Island: 4.244817374136229%

	Arrests by race
	White: 3.257650542941757%
	Black: 61.5004935834156%
	White Hispanic: 20.03948667324778%
	Black hispanic: 13.129318854886476%
	Asian: 1.7769002961500493%
	Native: 0.0

	Level of Offense charged
	Felony: 100.0%
	Misdemeanor: 0.0%
	Violation: 0.0

	job 1b230b35-f12d-4d9c-9872-2b89bcc32daa was completed at 2020-05-11 14:14:25.113816
	it took 0:00:00.284571 to complete your request

plot_data

	your plot was created and was saved to ./plots/chart_jobID=f9ac5d23-ca77-4239-9f82-f0c0fcc8b9e9.svg
	job f9ac5d23-ca77-4239-9f82-f0c0fcc8b9e9 was completed at 2020-05-11 14:17:30.346751
	it took 0:00:14.581475 to complete your request
	
Arrest_by_date

	...
	
	Arrest key: 206842120
	Arrest Date: 12/25/2019
	Crime Committed: ROBBERY
	Level of Offense: F
	Place of Arrest: M
	Sex: M
	Race: BLACK

	Arrest key: 206842472
	Arrest Date: 12/25/2019
	Crime Committed: FELONY ASSAULT
	Level of Offense: F
	Place of Arrest: K
	Sex: M
	Race: BLACK

	There were 414 arrests between 2019-12-24 00:00:00 and 2019-12-25 00:00:00
	job da818b89-29c0-495d-bc5d-fdbd2e0e064a was completed at 2020-05-11 14:18:53.115364
	It took 0:00:10.058144 to complete your request
	
Arrest_by_race

	...
	
	Arrest Key: 191717212
	Arrest Date: 01/01/2019
	Crime Committed: BURGLARY
	Level of Offense: F
	Place of Arrest: M
	Sex: M
	Race: BLACK

	Arrest Key: 191719210
	Arrest Date: 01/01/2019
	Crime Committed: OFFENSES AGAINST PUBLIC ADMINI
	Level of Offense: M
	Place of Arrest: K
	Sex: M
	Race: BLACK

	There were 102585 arrests of BLACK people
	BLACK people make up 0.4779910258739988% of all arrests in 2019
	job 81a1009c-caa4-4540-a47a-1e63ba6e46c5 was completed at 2020-05-11 14:20:22.753850
	It took 0:00:17.913654 to complete your request
	
New_Arrest

	The Arrest key for the new arrest is 106852494
	Jobe75f2518-2852-49b3-8e90-e07fb87cce87 was completed at 2020-05-11 14:22:28.681007
	It took 0:00:13.073382 to complete your request
	
curl localhost:5000/get_arrest/106852494

	 Arrest Key: 106852494
	Arrest Date: 07/04/2019
	Crime Committed: MURDER & NON-NEGL. MANSLAUGHTE
	Level of Offense: F
	Place of Arrest: B
	Sex: M
	Race: WHITE

	Job 2f6504ba-98ce-4e41-b571-f415b96244d4 was completed at 2020-05-11 14:23:57.195724
	It took 0:00:00.074583 to complete your request
	
curl localhost:5000/casecount/10

	There were 4451 arrests for BURGLARY
	Job 6fbee415-fb72-4cb6-9ba3-b6c6df0bdaf9 was completed at 2020-05-11 14:25:10.558165
	it took 0:00:00.306510 to complete your request
	
	
Commercialization of this app:

	I believe this application, though a bit clunky at the moment, could have the potential
	to become a widely used tool by both the academic and consumer markets. Should further 
	effort be put into making the application complatible with several large open arrest 
	datasets then this app could be useful to research institutions who wish to search for
	and recognize trends in modern arrest data. With increased statistical functionality
	this app could potentially recognize trends on its own from which statisticians can 
	interperet and draw conclusions from these trends. As mentioned earlier this app could 
	also appeal to a consumer market targetting people who are interested in learning about
	and messing around with real world data. If this app did reach a wide consumer audience then a
	possible hidden effect could be an increased level of scrutiny on law enforcement. Though
	this may sound bad the truth is that if a large group of people are made aware of the 
	insane proportion of arrests that black people make up in NYC, then maybe the NYPD will 
	experience external pressure to address this issue. These are just some of the ways this app
	could be marketed to a consumer audience and with increased functionality and optimization
	this app could become a very useful tool

How to make money?

	As a person who detests advertisements, I believe the primary way in which I would make 
	money off of this app is through some kind of monthly subscription fee like photoshop
	or many other computer applications

How can this app be better?

	My biggest issue with my app is the time in which it takes to complete some requests.
	For some routes the increased time is as a result of the large amounts of data that is printed out.
	for these routes I would prefer not to print the output and to just make the output 
	dictionary available to the user for them to dig through. However, for the sake of this
	assignment I have chosen to let the output print out in spite of this increased time.
	For some routes though the long wall clock time can be attributed to expensive programming.
	In the future I will make a point of limitting the amount of for loops present in my code.
	With such a large dataset, looping throug its entirety is very expensive and takes a long time,
	especially when it looped through multiple times. Regardless I am still very proud of my
	app and hope you all will be as well. Enjoy.