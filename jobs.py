from flask import Flask, request, json, jsonify
import redis, pygal
from hotqueue import HotQueue
from datetime import datetime

rd = redis.StrictRedis(host='127.0.0.1', port='6379', db =1)
q = HotQueue('queue', host='127.0.0.1', port='6379', db =2)

with open('NYC_Arrests.txt', 'r') as arrest_data:
	Data = json.load(arrest_data)

with open('Crimes.txt', 'r') as crime_list:
	Crimes = json.load(crime_list)

app = Flask(__name__)

#jobs.py does the app's backend heavy lifting, crunching all the numbers and creeating the user's output

@app.route('/get_arrest', methods= ['GET','POST']) # route corresponding to that of the same name in app
def get_arrest():
	if request.method == 'POST':
		your_arrest = {}
		your_arrest['Arrests'] = [] # initialize json dictionary to store output in
		arrest_key = request.form.get('Arrest Key') # get parameters from worker.py
		for i in Data['Arrests']:
			if i['Arrest Key'] == arrest_key: # iterate through dictionary until request arrest is located
				your_arrest['Arrests'].append(i) # add arrest to output dict
		print(your_arrest)
		return jsonify(your_arrest) # return output dict in json format

@app.route('/casecount', methods = ['GET','POST'])
def casecount():
	if request.method == 'POST':
		crime_index = request.form.get('crime') # get parameters from worker.py
		count = 0 # initailize casecount
		for i in Data['Arrests']: # iterate through arrest dict 
			if i['Crime Committed'] == Crimes[int(crime_index)]: 
				count = count+1 # if the crime matches the one we want, add to the casecount
		return str(count) # return casecount as output

@app.route('/ArrestByDate',  methods= ['GET','POST'])
def ArrestByDate():
	if request.method == 'POST':
		Arrests_in_range = {}
		Arrests_in_range['Arrests'] = [] # initialize output dict
		date_1 = request.form.get('date_1') # get parameters from worker.py
		date_2 = request.form.get('date_2')
		for j in reversed(Data['Arrests']): # iterate through reversed arrest dict since the arrest are order most to least recent
			date = str(j['Arrest Date']) # pull arrest date from Data['Arrests'][j]
			if date is not None: # some arrests do not have a date so this avoids crashing
				format_str = '%m/%d/%Y'
				arrest_date = str(datetime.strptime(date, format_str)) # change format of dates in the arrest dict to match those of the user input for accurate comparison
				if arrest_date >= str(date_1) and arrest_date <= str(date_2):
					Arrests_in_range['Arrests'].append(j) # if the arrest date falls within the user specified range then it is added to the output dict

		return jsonify(Arrests_in_range) # return output dict in json format

@app.route('/ArrestByRace', methods=['GET','POST'])
def ArrestByRace():
	if request.method == 'POST':
		Arrests_by_race = {}
		Arrests_by_race['Arrests'] = [] #initialize output dict
		race = request.form.get('race')
		print(race)
		for j in Data['Arrests']:
			if j['Race'] == race:
				Arrests_by_race['Arrests'].append(j) # if races match, add arrest to output dict

	return jsonify(Arrests_by_race) # return output dict

@app.route('/Arrest_statistics', methods= ['GET','POST'])
def arrest_stats():
	if request.method == 'POST':
		Arrest_statistics = []
		crime_index = request.form.get('crime_index')
		count = 0
		places = [] #initialize lists to hold information about arrests matching the input crime
		level_of_offense = []
		genders = []
		races = []
		for i in Data['Arrests']:
			if i['Crime Committed'] == Crimes[int(crime_index)]: # if the crime matches the user specified crime, add several other variable to the initialized lists
				count = count+1
				loc_of_arrest = i['Place of Arrest']
				level_ofs = i['Level of Offense']
				perp_sex = i['Sex']
				race = i['Race']
				level_of_offense.append(level_ofs)
				places.append(loc_of_arrest)
				genders.append(perp_sex)
				races.append(race)
		
		crime = Crimes[int(crime_index)] 
	#using the count() method we are able to create figure out the distribution of arrests across several variables like race, location of arrest, sex, etc
		pct_total_arrests = count/int(len(Data['Arrests']))
		pct_female = genders.count('F')/count
		pct_male = genders.count('M')/count

		pct_brooklyn = places.count('K')/count
		pct_bronx = places.count('B')/count
		pct_manhattan = places.count('M')/count
		pct_statenisland = places.count('S')/count
		pct_queens = places.count('Q')/count

		pct_felony = level_of_offense.count('F')/count
		pct_misdemeanor = level_of_offense.count('M')/count
		pct_violation = level_of_offense.count('V')/count

		pct_white = races.count('WHITE')/count
		pct_black = races.count('BLACK')/count
		pct_black_hispanic = races.count('BLACK HISPANIC')/count
		pct_white_hispanic = races.count('WHITE HISPANIC')/count
		pct_asian = races.count('ASIAN / PACIFIC ISLANDER')/count
		pct_native = races.count('AMERICAN INDIAN/ALASKAN NATIVE')/count

		Arrest_statistics = { # create output in json key-value pair format
			'crime' : crime,
			'count' : count,
			'pct_total' : pct_total_arrests,
			'pct_female' : pct_female,
			'pct_male' : pct_male,
			'pct_brooklyn' : pct_brooklyn,
			'pct_bronx' : pct_bronx,
			'pct_manhattan' : pct_manhattan,
			'pct_statenisland' : pct_statenisland,
			'pct_queens' : pct_queens,
			'pct_felony' : pct_felony,
			'pct_misdemeanor' : pct_misdemeanor,
			'pct_violation' : pct_violation,
			'pct_black' : pct_black,
			'pct_white' : pct_white,
			'pct_white_hispanic' : pct_white_hispanic,
			'pct_black_hispanic' : pct_black_hispanic,
			'pct_asian' : pct_asian,
			'pct_native' : pct_native}

	return jsonify(Arrest_statistics) # return output is json format

@app.route('/plot_data', methods=['GET','POST']) # use pygal to make pretty graphs for our users
def plot_data():
	if request.method == 'POST':
		dates = []
		for j in reversed(Data['Arrests']): # create the dates to label the x axis
			if j['Arrest Date'] not in dates:
				dates.append(j['Arrest Date'])

		jobID = request.form.get('jobID')
		method = request.form.get('method')
		variable = request.form.get('variable')
		Arrests_per_day = [] # create list to hold the y-value data 
		if method == 'race':
			for i in dates:
				count = 0
				for j in Data['Arrests']:
					if j['Race'] == variable and j['Arrest Date'] == i:
	# iterate first through the dates then through the arrests to find y-value data and fill the Arrests_per_day list
						count = count+1
				Arrests_per_day.append(count) 

		if method == 'borough':
			for i in dates:
				count = 0
				for j in Data['Arrests']:
					if j['Arrest Date'] == i and j['Place of Arrest'] == variable:
						count = count+1
				
				Arrests_per_day.append(count)
	# since the police database stores locations as single letters, I do this to make the plot prettier
			if variable == 'B':
				variable = 'The Bronx'
			elif variable == 'M':
				variable = 'Manhattan'
			elif variable == 'S':
				variable = 'Staten Island'
			elif variable == 'Q':
				variable = 'Queens'
			elif variable == 'K':
				variable = 'Brooklyn'
	
		if method == 'crime':
			for i in dates:
				crime = Crimes[int(variable)]
				count = 0
				for j in Data['Arrests']:
					if j['Arrest Date'] == i and j['Crime Committed'] == crime:
						count = count+1
				Arrests_per_day.append(count)
			variable = crime

	# create plot below using periodic x-axis date labels and a title that corresponds to the data being represented
		chart = pygal.Line(x_label_rotation=20, show_minor_x_labels=False, show_legend=False)
		chart.title = str('Arrests per day for chosen variables\n'+ variable)
		N = 30
		chart.x_labels = dates
		chart.x_labels_major = dates[::N]
		chart.add(variable, Arrests_per_day)
		chart_url = str('./plots/chart_jobID=' + jobID + '.svg') # create unique url for each customer's plot to be saved as. I couldn't figure out how to send a plot through html so I figured this was the next best thing
		chart.render_to_file(chart_url)

		return chart_url

@app.route('/New_Arrest', methods= ['GET', 'POST'])
def new_arrest():
	if request.method == 'POST': # get all parameters/data about the new arrest from worker.py
		arrest_key = request.form.get('Arrest Key')
		arrest_date = request.form.get('Arrest Date')
		crime_committed = request.form.get('Crime Committed')
		level_of_off = request.form.get('Level of Offense')
		place_of_arrest = request.form.get('Place of Arrest')
		sex = request.form.get('Sex')
		race = request.form.get('Race')
		New_Arrest = { # create new arrest in the json format of all other arrests in the database
			'Arrest Key' : arrest_key,
			'Arrest Date' : arrest_date,
			'Crime Committed' : Crimes[int(crime_committed)],
			'Level of Offense' : level_of_off,
			'Place of Arrest' : place_of_arrest,
			'Sex' : sex,
			'Race' : race}

		Data['Arrests'].append(New_Arrest) # add new arrest to the dictionary
		with open('NYPD_Arrests.txt', 'w') as output:
			json.dump(Data, output)	# save the new dictionary to NYPD_Arrests.txt
		
		return str(arrest_key)	# return the arrest key with which users can use the get_arrest route to see their newly posted arrest

if __name__ == '__main__':
	app.run(debug=True, host = '0.0.0.0')
