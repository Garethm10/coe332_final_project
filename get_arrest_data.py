from app import app
from hotqueue import HotQueue
import json
from datetime import *
import requests

#Urls for various flask endpoints

url_ABD = 'http://localhost:5000/ArrestByDate'
url_ABR = 'http://localhost:5000/ArrestByRace'
url_AS = 'http://localhost:5000/Arrest_statistics'
url_PD = 'http://localhost:5000/plot_data'
url_NA = 'http://localhost:5000/New_Arrest'

#This script is the primary way in which requests are made for the application
#The application has several commands that may be selected by users upon running the script

with open('Crimes.txt') as output:	#creates a dictionary of all crimes committed in 2019
	Crimes = json.loads(output.read())	#we will use this as a way of selecting which crime to inspect since a lot have odd or abreviated names that could be entered incorrectly

print('\nThis script has several command types to be used to access NYPD arrest data')
print('Commands: Arrest_by_date \nArrest_by_race \nArrest_statistics \nplot_data \nNew_Arrest')
print()
command_type = input('enter command you wish to enact: ')	#input command you wish to be enacted

if command_type == 'Arrest_by_date': 	#makes a request that will return all Arrests in a given date range
	print('\n This command will return all arrests within a range of dates')
	print('for all arrests on a single day enter the same date in both inputs \n \n')
	m1, d1, y1 = [ int(x) for x in input('enter first date (MM/DD/YYYY): ').split('/')]
	date1 = datetime(y1,m1,d1)	# convert input strings into datetime objects for comparison

	m2, d2, y2 = [ int(x) for x in input('enter second date (MM/DD/YYYY): ').split('/')]
	date2 = datetime(y2,m2,d2)

	payload = {	#create payload to be posted to app.py
		'date_1' : date1,
		'date_2' : date2}
	r = requests.post(url_ABD, data= payload) #create request

elif command_type == 'Arrest_by_race': 	#creates request for all Arrests of a certain race in 2019
	print('\nRaces: WHITE, BLACK, BLACK HISPANIC, WHITE HISPANIC, ASIAN / PACIFIC ISLANDER, \n AMERICAN INDIAN/ALASKAN NATIVE, UNKNOWN \n')
	
	race = input('Enter the race you wish to inspect exactly as it appears above: ')
	payload = {'race' : race}
	r = requests.post(url_ABR, data= payload)


elif command_type == 'Arrest_statistics':	#creates request for statistics of one of the crimes committed
	print('\nThis command will return basic statistics about a selected crime \n')
	for i in range(1,67):
		print(str(i) + '. ' + Crimes[i])	#provides user with list of crimes in which the index is equal to its place in the list
	print()
	print('Use the list of crimes provided to find the index of the offense you are curious about')
	crime = input('Enter crime index here: ')

	payload = {'crime' : crime}
	r = requests.post(url_AS, data= payload)

elif command_type == 'plot_data':	#creates request for a plot of a chosen variable over time
	print('\nThis route plots a chosen variable over the course of 2019\n')
	print('chose between plotting based on race, crime, or borough\n')
	method_ = input('enter which category you wish to select from for plotting (race, borough, crime): ') #select the category of the variable you wish to plot
	print('Crimes: \n')
	for i in range(1,67):
		print(str(i) + '. ' + Crimes[i])
	print()
	print('Races: \n')
	print('WHITE, BLACK, WHITE HISPANIC\nBLACK HISPANIC, ASIAN / PACIFIC ISLANDER\nAMERICAN INDIAN/ALASKAN NATIVE\n')
	print('Boroughs: \n')
	print('Brooklyn = K\nThe Bronx = B\nStaten Island = S\nQueens = Q\nManhattan = M\n')

	variable = input('enter variable you wish to see plotted: ') # enter actual variable to be plotted
	if method_ == 'race' or 'borough':
		payload = {'method' : method_,
			'variable' : variable}
	
	elif method_ == 'crime':
		variable = Crimes[int(variable)]
		print(variable)
		payload = {
			'method' : method_,
			'variable' : variable}

	else:
		'try again'
	
	r = requests.post(url_PD, data= payload)


elif command_type == 'New_Arrest':	#creates a post request to add a new arrest to the database
	print('\nThis route will allow you to post a new Arrest to the database\n')
	arrest_key = input('Enter the 9 digit arrest key for this new Arrest: ')
	arrest_date = input('Enter the date of arrest (MM/DD/2019): ')
	for i in range(1,67):
		print(str(i) + '. ' + Crimes[i])
	print()
	crime_committed = input('Enter the number that corresponds to the crime committed: ')
	level_of_off = input('Enter the level of offense of the crime (M/F/V): ')
	place_of_arrest = input('Enter the borough of arrest (M/B/K/S/Q): ')
	sex = input('Enter the sex of the perp (M/F): ')
	print('Races: WHITE, BLACK, BLACK HISPANIC \nWHITE HISPANIC, ASIAN / PACIFIC ISLANDER, \nAMERICAN INDIAN/ALASKAN NATIVE, UNKNOWN\n')
	race = input('Enter the race of the perp: ')
	payload = {	# all input should take the exact form of the examples provided
		'Arrest Key' : arrest_key,
		'Arrest Date' : arrest_date,
		'Crime Committed' : crime_committed,
		'Level of Offense' : level_of_off,
		'Place of Arrest' : place_of_arrest,
		'Sex' : sex,
		'Race' : race}

	r = requests.post(url_NA, data= payload)

print(r.text)
