import redis, json, requests
from hotqueue import HotQueue
from datetime import datetime
from flask import jsonify

rd = redis.StrictRedis(host='127.0.0.1', port ='6379', db=1)
q = HotQueue('queue', host='127.0.0.1', port='6379', db=2)

with open('NYC_Arrests.txt', 'r') as output: #initialize dataset
	Data = json.load(output)

with open('Crimes.txt', 'r') as output2: # initialize list of crimes committed in 2019
	Crimes = json.load(output2)

# the workers use the request tickets created by app.py to actually complete the jobs and store/print out the requests output

@q.worker
def do_work(jobID):
	job = json.loads(rd.get(jobID)) # get the ticket that corresponds to the jobID from redis
	if job['command_type'] == 'casecount': # check which command is to be executed
		start_time = datetime.now() # create start time object
		job['start_time'] =  str(start_time) # save start time as str in job ticket
		job['job_status'] = 'working' # status set to working and the job begins
		crime_index = job['parameters'] # parameters are set to a variable
		r = requests.post('http://localhost:5002/casecount', data= {'crime' : crime_index})
		# the request is made to jobs.py to return the customers desired output
		count = r.text # in the case of this route, the output is just text thus r.text gives us our output
		job['output'] = str(count) # output is set in job ticket
		job['job_status'] = 'complete' # job status is updated
		end_time = datetime.now() # end time object is created
		job['end_time'] = str(end_time) # end time saved as a str in the ticket
		rd.set(jobID, json.dumps(job)) # the job is placed back in redis completed
		time_elapsed = end_time - start_time # crude wall clock time
		print('There were '+ job['output']+ ' arrests for ' + Crimes[int(crime_index)])
		print('Job '+ job['jobID']+ ' was completed at ' + job['end_time'])
		print('it took ' + str(time_elapsed) + ' to complete your request')
		print()

	if job['command_type'] == 'ArrestByDate':
		start_time = datetime.now()
		job['start_time'] = str(start_time)
		job['job_status'] = 'working'
		dates = job['parameters']
		r = requests.post('http://localhost:5002/ArrestByDate', data= dates)
		output = r.json() # output is of a json format thus can be accessed with r.json()
		count = 0
		for i in output['Arrests']: # print the requests output to the user
			print('Arrest key: ' + i['Arrest Key'])
			print('Arrest Date: ' + i['Arrest Date'])
			print('Crime Committed: ' + i['Crime Committed'])
			print('Level of Offense: ' + i['Level of Offense'])
			print('Place of Arrest: ' + i['Place of Arrest'])
			print('Sex: ' + i['Sex'])
			print('Race: ' + i['Race'])
			print()
			count = count+1 # gather data about total arrests within the date range
		job['output'] = output
		job['job_status'] = 'complete'
		end_time = datetime.now()
		job['end_time'] = str(end_time)
		rd.set(jobID, json.dumps(job))
		time_elapsed = end_time - start_time
		print('There were ' + str(count) + ' arrests between ' + str(dates['date_1']) + ' and ' + str(dates['date_2']))
		print('job ' + job['jobID'] + ' was completed at ' + job['end_time'])
		print('It took ' + str(time_elapsed) + ' to complete your request')
		print()

	if job['command_type'] == 'ArrestByRace':
		start_time = datetime.now()
		job['start_time'] = str(start_time)
		job['job_status'] = 'working'
		race = job['parameters']
		r = requests.post('http://localhost:5002/ArrestByRace', data= {'race': race})
		output = r.json() # once again output is of json format
		count = 0
		for i in output['Arrests']: # print request's output to the user
			print('Arrest Key: ' + i['Arrest Key'])
			print('Arrest Date: ' + i['Arrest Date'])
			print('Crime Committed: ' + i['Crime Committed'])
			print('Level of Offense: ' + i['Level of Offense'])
			print('Place of Arrest: ' + i['Place of Arrest'])
			print('Sex: ' + i['Sex'])
			print('Race: ' + i['Race'])
			print()
			count = count+1
		job['output'] = output
		job['job_status'] = 'complete'
		end_time = datetime.now()
		job['end_time'] = str(end_time)
		rd.set(jobID, json.dumps(job))
		time_elapsed = end_time - start_time
		n = len(Data['Arrests'])
		pct = count/n # basica statistics about the returned output
		print('There were ' + str(count) + ' arrests of ' + str(race) + ' people')
		print(str(race) + ' people make up ' + str(pct) + '% of all arrests in 2019')
		print('job ' + job['jobID'] + ' was completed at ' + job['end_time'])
		print('It took ' + str(time_elapsed) + ' to complete your request')
		print()

	if job['command_type'] == 'Arrest_statistics':
		start_time = datetime.now()
		job['start_time'] = str(start_time)
		job['job_status'] = 'working'
		crime_index = job['parameters']
		r = requests.post('http://localhost:5002/Arrest_statistics', data= {'crime_index' : crime_index})
		output = r.json() # output is of json format
		print() # what follows is an ugly code that prints some pretty stats to the user
		print(output['crime'] + ' arrests made up ' + str(100*output['pct_total']) + '% of all NYPD arrests in 2019\nTotal arrests: '+ str(output['count']))
		print('\nArrests by gender\nMale: ' + str(100*output['pct_male']) + '%\nFemale: '+ str(100*output['pct_female']))
		print()
		print('Arrests by borough\nManhattan: '+ str(100*output['pct_manhattan']) + '%\nThe Bronx: '+ str(100*output['pct_bronx']) + '%\nBrooklyn: ' + str(100*output['pct_brooklyn']) + '%\nQueens: '+ str(100*output['pct_queens']) + '%\nStaten Island: ' + str(100*output['pct_statenisland']) + '%' )	
		print()
		print('Arrests by race\nWhite: ' + str(100*output['pct_white']) + '%\nBlack: ' + str(100*output['pct_black']) + '%\nWhite Hispanic: ' + str(100*output['pct_white_hispanic']) + '%\nBlack hispanic: ' + str(100*output['pct_black_hispanic']) + '%\nAsian: ' + str(100*output['pct_asian']) + '%\nNative: ' + str(100*output['pct_native']))
		print()
		print('Level of Offense charged\nFelony: '+ str(100*output['pct_felony']) + '%\nMisdemeanor: ' + str(100*output['pct_misdemeanor']) + '%\nViolation: ' + str(100*output['pct_violation']))
		print()

		job['output'] = output
		job['job_status'] = 'complete'
		end_time = datetime.now()
		job['end_time'] = str(end_time)
		rd.set(jobID, json.dumps(job))
		time_elapsed = end_time - start_time
		print('job '+ job['jobID'] + ' was completed at ' + job['end_time'])
		print('it took ' + str(time_elapsed) + ' to complete your request')
		print()

	if job['command_type'] == 'plot_data':
		start_time = datetime.now()
		job['start_time'] = str(start_time)
		job['job_status'] = 'working'
		params = job['parameters'] # params in this case is actually a dictionary of parameters
		r = requests.post('http://localhost:5002/plot_data', data = params) # params is passed as is instead of with a correspoding key since params is alread in key-value pair form
		output = r.text # output is the filename of the plot, the plot will be stored in ./plots
		print('your plot was created and was saved to ' + output)
		job['output'] = output
		job['job_status'] = 'complete'
		end_time = datetime.now()
		job['end_time'] = str(end_time)
		rd.set(jobID, json.dumps(job))
		time_elapsed = end_time - start_time
		print('job ' + jobID + ' was completed at ' + job['end_time'])
		print('it took ' + str(time_elapsed) + ' to complete your request')
		print()

	if job['command_type'] == 'New_Arrest':
		start_time = datetime.now()
		job['start_time'] = str(start_time)
		job['job_status'] = 'working'
		params = job['parameters'] # once again params is a dictionary of key-value pairs
		r = requests.post('http://localhost:5002/New_Arrest', data = params)
		output = r.text #ouput is the new arrests arrest key, restarting jobs.py and then using curl localhost:5000/get_arrest/<arrest key> with return your posted arrest
		print('The Arrest key for the new arrest is ' + output)
		job['output'] = output
		job['job_status'] = 'complete'
		end_time = datetime.now()
		job['end_time'] = str(end_time)
		rd.set(jobID, json.dumps(job))
		time_elapsed = end_time - start_time
		print('Job' + jobID + ' was completed at ' + job['end_time'])
		print('It took ' + str(time_elapsed) + ' to complete your request')
		print()

	if job['command_type'] == 'get_arrest':
		start_time = datetime.now()
		job['start_time'] = str(start_time)
		job['job_status'] = 'working'
		arrest_key = job['parameters']
		r = requests.post('http://localhost:5002/get_arrest', data = {'Arrest Key' : arrest_key})
		output = r.json() # output is the arrest in json format
		for i in output['Arrests']: # display arrest to the user
			print('Arrest Key: '+ i['Arrest Key'])
			print('Arrest Date: ' + i['Arrest Date'])
			print('Crime Committed: ' + i['Crime Committed'])
			print('Level of Offense: ' + i['Level of Offense'])
			print('Place of Arrest: ' + i['Place of Arrest'])
			print('Sex: ' + i['Sex'])
			print('Race: ' + i['Race'])
			print()
		job['output'] = output
		job['job_status'] = 'complete'
		end_time = datetime.now()
		job['end_time'] = str(end_time)
		rd.set(jobID, json.dumps(job))
		time_elapsed = end_time - start_time
		print('Job '+ jobID + ' was completed at '+ job['end_time'])
		print('It took ' + str(time_elapsed) + ' to complete your request')
		print()

do_work()
