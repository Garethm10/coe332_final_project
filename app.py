from flask import Flask, request, json, jsonify
import redis
from hotqueue import HotQueue
from datetime import datetime
import uuid

rd = redis.StrictRedis(host='127.0.0.1', port='6379', db =1)
q = HotQueue('queue', host='127.0.0.1', port='6379', db =2)

app = Flask(__name__)

datafile = 'NYC_Arrests.txt'
with open(datafile) as output:
	Data = json.loads(output.read())

# app.py handles the requests made by curls or get_arrest_data.py by creating a unique job ticket to be sent to worker.py

@app.route('/', methods=['GET']) # basic route to start the application
def welcome():
	return '\nWelcome to the 2020 NYPD Arrests Database \n \n'

@app.route('/get_arrest/<arrest_key>', methods=['GET'])	#this route is accessed by curl request and uses the query parameter arrest key to return the corresponding arrest that matched the key
def get_arrest(arrest_key):
	jobID = str(uuid.uuid4()) # create unique identifier for this specific request
	job_status = 'requested' # set current job status
	command_type = 'get_arrest' #set the command type that corresponds the request
	parameters = arrest_key # set parameters that worker.py and jobs.py will use the return data
	timestamp = datetime.now() # create timestamp for the request
	start_time = [] # initalize start time on ticket, this will be updated by worker.py
	end_time = [] # initialize end time to be updated by worker.py
	output = [] # initialize object that will hold the output of our request
	job = {	# create job ticket
		'jobID' : jobID,
		'job_status' : job_status,
		'command_type' : command_type,
		'parameters' : parameters,
		'timestamp' : timestamp,
		'start_time' : start_time,
		'end_time' : end_time,
		'output' : output}
	rd.set(jobID, json.dumps(job)) # add ticket to redis database with the key being the jobs uuid
	q.put(jobID) # add jobs unique uuid to queue, worker.py will pull this from the queue and retrieve the corresponding ticket from redis
	return '\nYour job has been queued\n'

@app.route('/casecount/<crime_index>', methods=['GET']) # curl accessible route that return total number of arrests made for a chosen crime
def casecount(crime_index): # takes query parameter crime_index which is the index of the crime in our crimes dictionary
	jobID = str(uuid.uuid4()) 
	job_status = 'requested'
	command_type = 'casecount'
	parameters = crime_index
	timestamp = datetime.now()
	start_time = []
	end_time = []
	output = []
	job = {
		'jobID' : jobID,
		'job_status' : job_status,
		'command_type' : command_type,
		'parameters' : parameters,
		'timestamp' : timestamp,
		'start_time' : start_time,
		'end_time' : end_time,
		'output' : output}
	rd.set(jobID, json.dumps(job))
	q.put(jobID)
	return '\n Your job has been queued \n'

@app.route('/ArrestByDate', methods=['GET','POST']) # accessible by get_arrest_data.py
def arrestbydate():
	if request.method == 'POST':
		jobID = str(uuid.uuid4())
		job_status = 'requested'
		command_type = 'ArrestByDate'
		parameters = {
			'date_1' : request.form.get('date_1'), # gets parameters from get_arrest_data
			'date_2' : request.form.get('date_2')} # creates a mini parameter dict
		timestamp = datetime.now()
		start_time = []
		end_time = []
		output = []
		job = {
			'jobID' : jobID,
			'job_status' : job_status,
			'command_type' : command_type,
			'parameters' : parameters,
			'timestamp' : timestamp,
			'start_time' : start_time,
			'end_time' : end_time,
			'output' : output}
		rd.set(jobID, json.dumps(job))
		q.put(jobID)
	return '\n Your job has been queued \n'

@app.route('/ArrestByRace', methods=['GET','POST']) # accessible by get_arrest_data.py
def arrestbyrace():
	if request.method == 'POST':
		jobID = str(uuid.uuid4())
		job_status = 'requested'
		command_type = 'ArrestByRace'
		parameters = request.form.get('race') # gets parameters from get_arrest_data.py
		timestamp = datetime.now()
		start_time = []
		end_time = []
		output = []
		job = {
			'jobID' : jobID,
			'job_status' : job_status,
			'command_type' : command_type,
			'parameters' : parameters,
			'timestamp' : timestamp,
			'start_time' : start_time,
			'end_time' : end_time,
			'output' : output}
		rd.set(jobID, json.dumps(job))
		q.put(jobID)
	return '\n Your job has been queued \n'

@app.route('/Arrest_statistics', methods=['GET','POST']) # accessible with get_arrest_data.py
def arrest_stats():
	if request.method == 'POST':
		jobID = str(uuid.uuid4())
		job_status = 'requested'
		command_type = 'Arrest_statistics'
		parameters = request.form.get('crime') # gets parameters from get_arrest_data.py
		timestamp = datetime.now()
		start_time = []
		end_time = []
		output = []
		job = {
			'jobID' : jobID,
			'job_status' : job_status,
			'command_type' : command_type,
			'parameters' : parameters,
			'timestamp' : timestamp,
			'start_time' : start_time,
			'end_time' : end_time,
			'output' : output}
		rd.set(jobID, json.dumps(job))
		q.put(jobID)
	return '\nYour job has been queued \n'

@app.route('/plot_data', methods=['GET','POST']) # accessible with get_arrest_data.py
def plot_data():
	if request.method == 'POST':
		jobID = str(uuid.uuid4())
		job_status = 'requested'
		command_type = 'plot_data'
		parameters = {
			'method' : request.form.get('method'),
			'variable' : request.form.get('variable'),
			'jobID' : jobID} #passes jobID as a parameter as it will be used to name the output file that the plot will be saved as
		timestamp = datetime.now()
		start_time = []
		end_time = []
		output = []
		job = {
			'jobID' : jobID,
			'job_status' : job_status,
			'command_type' : command_type,
			'parameters' : parameters,
			'timestamp' : timestamp,
			'start_time' : start_time,
			'end_time' : end_time,
			'output' : output}
		rd.set(jobID, json.dumps(job))
		q.put(jobID)
	return '\nYour job has been queued\n'

@app.route('/New_Arrest', methods=['GET','POST']) # accessible with get_arrest_data.py
def new_arrest():
	if request.method == 'POST':
		jobID = str(uuid.uuid4())
		job_status = 'requested'
		command_type = 'New_Arrest'
		parameters = {
			'Arrest Key' : request.form.get('Arrest Key'), #all inputs must follow
			'Arrest Date' : request.form.get('Arrest Date'), # conventions used in the 
			'Crime Committed' : request.form.get('Crime Committed'), # original dataset
			'Level of Offense' : request.form.get('Level of Offense'), # as further 
			'Place of Arrest' : request.form.get('Place of Arrest'), # detailed in the
			'Sex' : request.form.get('Sex'), # README
			'Race' : request.form.get('Race')}
		timestamp = datetime.now()
		start_time = []
		end_time = []
		output = []
		job = {
			'jobID' : jobID,
			'job_status' : job_status,
			'command_type' : command_type,
			'parameters' : parameters,
			'timestamp' : timestamp,
			'start_time' : start_time,
			'end_time' : end_time,
			'output' : output}
		rd.set(jobID, json.dumps(job))
		q.put(jobID)
	return '\nYour job has been queued\n'

if __name__ == '__main__':
	app.run(debug=True, host ='0.0.0.0')
